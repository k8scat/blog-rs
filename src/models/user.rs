use anyhow::Result;

pub struct User {
    pub id: i32,
    pub name: String,
    pub email: String,
    pub password: String,
}

pub fn get_user(id: i32) -> Result<User> {
    let user = User {
        id: 1,
        name: String::from("John"),
        email: String::from(""),
        password: String::from(""),
    };
    Ok(user)
}

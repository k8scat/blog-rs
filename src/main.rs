mod models;
mod controllers;
mod services;

use actix_web::{App, HttpServer};
use controllers::user;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().service(user::index))
        .bind("127.0.0.1:8080")?
        .run()
        .await
}
